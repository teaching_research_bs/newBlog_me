#coding=utf-8
__author__ = u'坤子'
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from mysite.models import Blog

class LatestEntriesFeed(Feed):
    title = u"坤子的技术博客"            # title
    link = "http://blog.xuyukun.com/"       # 首页链接
    feed_url='http://blog.xuyukun.com/'
    feed_guid='http://blog.xuyukun.com/'
    author_name='坤子'
    description = "专注于pyton、常用Python开源程序及Python WEB 开源框架在线文档,Django,Flask,Uliweb."

    def items(self):
        """数据源对象"""
        return Blog.objects.filter(is_show__isnull=True).order_by('-add_date')[:10]       # 订阅最新10篇

    def item_title(self, item):
        """数据源标题"""
        return item.title

    def item_description(self, item):
        """数据源内容"""
        return item.rss

    def item_link(self, item):
        """数据源链接地址"""
        return 'http://blog.xuyukun.com'+reverse('blog', args=[item.id])

    def item_categories(self, item):
        """数据源分类"""
        return  (item.getType().name, )

    def get_absolute_url(self):
        return 'http://blog.xuyukun.com/'